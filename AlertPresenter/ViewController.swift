//
//  ViewController.swift
//  AlertPresenter
//
//  Created by Anatolii Kyryliuk on 20/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private let alertPresenter: AlertPresenterProtocol = AlertPresenter()

    @IBOutlet weak var resultLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showAlertButtonTapped() {
        showAlert()
    }

    private func showAlert() {
        let title = "Title"
        let message = "Message"
        let okAction = UIAlertAction(title: "OK", style: .default, handler: okActionHandler)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: cancelActionHandler)

        alertPresenter.showAlert(on: self, title: title, message: message, actions: [okAction, cancelAction])
    }

    private func okActionHandler(action: UIAlertAction) {
        updateResultLabel(with: action.title, color: UIColor.blue)
    }

    private func cancelActionHandler(action: UIAlertAction) {
        updateResultLabel(with: action.title, color: UIColor.red)
    }

    private func updateResultLabel(with title: String?, color: UIColor) {
        guard let title = title else { return }

        resultLabel.text = "\(title) action pressed"
        resultLabel.textColor = color
    }
}

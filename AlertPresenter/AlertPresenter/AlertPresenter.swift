//
//  AlertPresenter.swift
//  AlertPresenter
//
//  Created by Anatolii Kyryliuk on 20/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit

protocol AlertPresenterProtocol {
    func showAlert(on viewController: UIViewController, title: String, message: String, actions: [UIAlertAction])
}

class AlertPresenter: AlertPresenterProtocol {
    func showAlert(on viewController: UIViewController, title: String, message: String, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach {
            alert.addAction($0)
        }

        viewController.present(alert, animated: true, completion: nil)
    }
}

//
//  AlertPresenterUITests.swift
//  AlertPresenterUITests
//
//  Created by Anatolii Kyryliuk on 20/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import XCTest

class AlertPresenterUITests: XCTestCase {
    var app: XCUIApplication!
    let kPredicateFormat = "label BEGINSWITH '%@'"

    override func setUp() {
        super.setUp()

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAlert() {
        app.launch()
        let okText = "OK"
        let cancelText = "Cancel"
        let kShowAlertText = "Show Alert"

        // show alert
        app.buttons[kShowAlertText].tap()

        // check actions
        XCTAssert(app.buttons[okText].exists)
        XCTAssert(app.buttons[cancelText].exists)
        app.buttons[okText].tap()

        // check action result
        sleep(1)
        var predicate = NSPredicate(format: String(format: kPredicateFormat, okText))
        XCTAssert(app.staticTexts.element(matching: predicate).exists)

        // show alert
        app.buttons[kShowAlertText].tap()

        // check actions
        XCTAssert(app.buttons[okText].exists)
        XCTAssert(app.buttons[cancelText].exists)
        app.buttons[cancelText].tap()

        // check action result
        sleep(1)
        predicate = NSPredicate(format: String(format: kPredicateFormat, cancelText))
        XCTAssert(app.staticTexts.element(matching: predicate).exists)
    }
}

//
//  MockViewController.swift
//  AlertPresenterTests
//
//  Created by Anatolii Kyryliuk on 20/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import UIKit
import XCTest

class MockViewController: UIViewController {
    var isPresentMethodCalled: Bool = false
    var alertTitle: String?
    var alertMessage: String?
    var alertActions: [UIAlertAction]?

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        guard let alertController = viewControllerToPresent as? UIAlertController else {
            return
        }

        isPresentMethodCalled = true
        alertTitle = alertController.title
        alertMessage = alertController.message
        alertActions = alertController.actions
    }
}

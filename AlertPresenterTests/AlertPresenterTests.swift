//
//  AlertPresenterTests.swift
//  AlertPresenterTests
//
//  Created by Anatolii Kyryliuk on 20/09/2018.
//  Copyright © 2018 Test. All rights reserved.
//

import XCTest
@testable import AlertPresenter

class AlertPresenterTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAlertPresenter() {
        let alertPresenter = AlertPresenter()
        let mocController = MockViewController()

        let title = "Title"
        let message = "Message"
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        let actions = [okAction]

        alertPresenter.showAlert(on: mocController, title: title, message: message, actions: [okAction])

        XCTAssert(mocController.isPresentMethodCalled)
        XCTAssert(mocController.alertTitle == title)
        XCTAssert(mocController.alertMessage == message)
        if let alertActions = mocController.alertActions {
            XCTAssert(actions.count == alertActions.count)
            XCTAssert(actions[0].title == actions[0].title)
        } else {
            XCTAssert(false, "Empty Alert Actions")
        }
    }
}
